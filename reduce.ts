import fetch from 'node-fetch';
import { TStudyGroupUserType, EInterest, TList } from './reduce.type';


/** 1. 배열 안의 수를 더해야 할 경우 */ 

const numberArray: number[] = [0, 1, 2, 3, 4, 5];
let sum = 0;

// 1 - for loop 을 사용했을 시
for(const number of numberArray) {
  sum += number;
}
console.log('for loop: ', sum);

// 2 - Reduce를 사용했을 경우
sum = numberArray.reduce((accu, curr) => accu + curr);
console.log('reduce', sum);




/** 2. string[] 형태의 인스턴스 갯수 세기 */ 

// async await 사용하기 위한 IIFE
(async () => {
  // 1 - data fetching
  const fruits: string[] = await fetch('http://localhost:3000/fruits').then(data => data.json());


  // 2 - reduce
  let fruitsResult = fruits.reduce<{ [key: string]: number }>((fruitCount, fruitName) => {

    // if (fruitCount[fruitName]) {
    //   fruitCount[fruitName]++;
    // } else {
    //   fruitCount[fruitName] = 1;
    // }
    fruitCount[fruitName] ? fruitCount[fruitName]++ : fruitCount[fruitName] = 1;

    return fruitCount;
  }, {});

  console.table(fruitsResult);
})();


/** 3. 중첩 배열 펼치기. */ 

const arrayOfArrays = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];

let oneDimensionalArray = arrayOfArrays.reduce((accu, curr) => accu.concat(curr), []);
console.log(oneDimensionalArray);


/** 4. pipe 함수를 이용해 함수 구성하기 */

// 1 - 미리 함수를 구현
const multiple = (num: number) => num * 2;
const triple = (num: number) => num * 3;
const quadruple = (num: number) => num * 4;

// 2 - 파이프 함수 구현
let pipe = (functions: Function[]) => (num: number) => 
  functions.reduce((accu, curr) => {
    return curr(accu);
  }, num);


console.log(pipe([multiple, triple, quadruple])(1));



// (async () => {
//   const initialValue: TList = Object.values(EInterest)
//     .filter(key => typeof key === 'string')
//     .map(key => ({ name: key, count: 0 }));

//   const users: TStudyGroupUserType[] = await fetch('http://localhost:3000/users').then(data => data.json());

//   const interestList = users.reduce<TList>((accumulator, currentValue) => {
//     const { interests = [] } = currentValue;

//     const interestsWithDuplicationRemoved = interests.sort().reduce<number[]>((accu, curr) => {
//       const length = accu.length;
//       if (length === 0 || accu[length - 1] !== curr) {
//         accu.push(curr);
//       }

//       return accu;
//     }, []);

//     interestsWithDuplicationRemoved.forEach(interest => {
//       const interestName = EInterest[interest];
//       const element = accumulator.find(f => f.name === interestName);

//       if (element) {
//         element.count += 1;
//       }

//     });
//     return accumulator;
//   }, initialValue);

//   const sortedInterestList = interestList.sort((a, b) => a.count < b.count ? 1 : -1);

//   console.table(sortedInterestList);
// })();


const faker = require('faker');

faker.locale = 'ko';

const randomFruit = (count = 0) => {
  const FRUIT_NAMES = ['banana', 'cherry', 'orange', 'apple', 'mango'];
  return new Array(count).fill(null).map(_ => FRUIT_NAMES[Math.floor(Math.random() * FRUIT_NAMES.length)]);
}


const getFruit = (count) => {
  return randomFruit(count);
}

/**
 * @param {number} [userCount]
 * 
 * @return {{ id: number, name: string, age: number, interests: number[] }[]} 
 */

const getUsersMockData = (userCount) => {
  const users = [];
  for (let i = 0; i < userCount; i++) {
    users.push({
      id: i,
      name: `${faker.name.lastName()}${faker.name.firstName()}`,
      age: faker.random.number(10) + 25,
      interests: new Array(faker.random.number(3)).fill(null).map(() => faker.random.number(7)),
    });
  }
  return users;
}

module.exports = () => {
  const data = {
    users: getUsersMockData(20),
    fruits: getFruit(20)
  };
  return data;
}
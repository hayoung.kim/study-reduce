export enum EInterest {
  javascript,
  typescript,
  react,
  vue,
  angular,
  redux,
  mobX,
  ES6,
};

export type TStudyGroupUserType = {
  id: number,
  name: string,
  age: number,
  interests: EInterest[];
}
export type TList = { name: string | EInterest, count: number }[];